// chatgpt: In Rust, you don't explicitly need to include an exit(0) statement at the end of your program.
// When a Rust program reaches the end of its main function without encountering any panics or errors,
// it implicitly returns 0, indicating successful execution.

// However, if you want to be explicit, you can use the std::process::exit function

fn main() {
    print!("hi\n");
}
