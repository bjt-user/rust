use std::fs;

fn main() {
    // Replace "." with the path to the directory you want to list
    let path = "/";

    match fs::read_dir(path) {
        Ok(entries) => {
            for entry in entries {
                if let Ok(entry) = entry {
                    println!("{}", entry.file_name().to_string_lossy());
                }
            }
        }
        Err(e) => eprintln!("Error reading directory: {}", e),
    }
}

