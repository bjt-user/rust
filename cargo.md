help
```
cargo help install
```

list installed crates
```
cargo install --list
```

search for packages in "crates.io":
```
$ cargo search rodio
    Updating crates.io index
rodio = "0.17.1"             # Audio playback library
rodio-xm = "0.1.1"           # provides a source for playback of fasttracker 2 extended modules in rodio
magnum = "1.0.0"             # Reader to Iterator/Source support for Opus Audio in common container formats. Includes optional tr…
atrac3p-decoder = "0.1.2"    # Atrac3+ Decoder in Rust. Can be used as a source for Rodio.
ffmpeg-decoder = "0.1.3"     # Decodes audio files using ffmpeg with rust. Can be used as a rodio source.
hodaun = "0.2.3"             # Audio IO and synthesis
rodio_wav_fix = "0.15.0"     # Audio playback library
ambisonic = "0.4.1"          # Compose and play 3D audio.
redlux = "0.6.0"             # AAC decoder for MPEG-4 (MP4, M4A etc) and AAC files, with rodio support
hps_decode = "0.1.1"         # A library for decoding Super Smash Bros. Melee music files
... and 32 crates more (use --limit N to see more)
```

```
$ cargo install rodio
    Updating crates.io index
  Downloaded rodio v0.17.1
  Downloaded 1 crate (57.9 KB) in 0.33s
error: there is nothing to install in `rodio v0.17.1`, because it has no binaries
`cargo install` is only for installing programs, and can't be used with libraries.
To use a library crate, add it as a dependency in a Cargo project instead.
```

#### adding a dependency

put this in your `Cargo.toml` file:
```
[dependencies]
rodio = "0.17.1"
```
then do
```
cargo build
```

This shows the dependency tree:
```
cargo tree
```
***
