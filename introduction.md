https://doc.rust-lang.org/stable/book/title-page.html

https://en.wikipedia.org/wiki/Rust_(programming_language)

First appeared	May 15, 2015\
=> very modern programming language

#### installation
```
sudo apt install rustc
```

```
sudo pacman -S rust
```

```
rustc --version
```

compile with
```
rustc hello_world.rs
``` 

```
fn main() {
    println!("Hello, world!");
}
```

***
